const express = require("express");
const functions = require(`../back/functions`);
var router = express.Router();
// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

router.get("/", (req, res) => {
  res.redirect("/inicio");
});

router.get("/inicio", (req, res) => {
  functions.loginGet(req, res);
});

router.post("/Horario", (req, res) => {
  functions.horarioPost(req, res);
});

router.get("/Horario", (req, res) => {
  functions.horarioGet(req, res);
});


module.exports = {
  router
};
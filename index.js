const express = require("express");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const cors = require("cors");
const bodyParser = require("body-parser");
var forceSsl = require("force-ssl-heroku");

var {
    router
} = require("./routes/router");

var port = process.env.PORT || 8000;


console.log("Iniciando servidor");

let app = express();
//app.use(forceSsl);

//Sirve para conocer los valores enviados mediante POST
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(
    bodyParser.urlencoded({
        extended: true
    })
); // support encoded bodies

//Carpeta que contiene archivos estáticos
app.use(express.static("public"));

//Manejo de cookies
app.use(cookieParser());

//Manejo de sesiones
app.use(
    session({
        secret: "ArkNeo",
        resave: false,
        saveUninitialized: true,
        cookie: {
            secure: false
        }
    })
);

//Enrutador de la peticiones HTTP realizadas
app.use(router);
/*
var https = require('https')
var fs = require("fs")
https.createServer({
        key: fs.readFileSync('./back/key.pem'),
        cert: fs.readFileSync('./back/cert.pem')
    }, app)
    .listen(port, function () {
        console.log("At " + port)
    })
var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(301, {
        "Location": "https://" + req.headers['host'] + req.url
    });
    res.end();
}).listen(80);
*/

app.listen(port, () => {
    console.log(`Servidor iniciado en el puerto: ${port}`);
});
var escuelas = [{
    URL: "http://www.saes.esimeazc.ipn.mx",
    Nombre: "ESIME AZCAPOTZALCO"
  },
  {
    URL: "http://www.saes.esimecu.ipn.mx",
    Nombre: "ESIME CULHUACAN"
  },
  {
    URL: "http://www.saes.esimetic.ipn.mx",
    Nombre: "ESIME TICOMÁN"
  },
  {
    URL: "http://www.saes.esimez.ipn.mx",
    Nombre: "ESIME ZACATENCO"
  },
  {
    URL: "http://www.saes.esiatec.ipn.mx",
    Nombre: "ESIA TECAMACHALCO"
  },
  {
    URL: "http://www.saes.esiatic.ipn.mx",
    Nombre: "ESIA TICOMÁN"
  },
  {
    URL: "http://www.saes.esiaz.ipn.mx",
    Nombre: "ESIA ZACATENCO"
  },
  {
    URL: "http://www.saes.cicsma.ipn.mx",
    Nombre: "CICS MILPA ALTA"
  },
  {
    URL: "http://www.saes.cicsst.ipn.mx",
    Nombre: "CISC ST"
  },
  {
    URL: "http://www.saes.escasto.ipn.mx",
    Nombre: "ESCA ST"
  },
  {
    URL: "http://www.saes.escatep.ipn.mx",
    Nombre: "ESCA TEPEPAN"
  },
  {
    URL: "http://www.saes.encb.ipn.mx",
    Nombre: "ENCB"
  },
  {
    URL: "http://www.saes.enmh.ipn.mx",
    Nombre: "ENMH"
  },
  {
    URL: "http://www.saes.eseo.ipn.mx",
    Nombre: "ESEO"
  },
  {
    URL: "http://www.saes.esm.ipn.mx",
    Nombre: "ESM"
  },
  {
    URL: "http://www.saes.ese.ipn.mx",
    Nombre: "ESE"
  },
  {
    URL: "http://www.saes.est.ipn.mx",
    Nombre: "EST"
  },
  {
    URL: "http://www.saes.upibi.ipn.mx",
    Nombre: "UPIBI"
  },
  {
    URL: "http://www.saes.upiicsa.ipn.mx",
    Nombre: "UPIICSA"
  },
  {
    URL: "http://www.saes.upiita.ipn.mx",
    Nombre: "UPIITA"
  },
  {
    URL: "http://www.saes.escom.ipn.mx",
    Nombre: "ESCOM"
  },
  {
    URL: "http://www.saes.esfm.ipn.mx",
    Nombre: "ESFM"
  },
  {
    URL: "http://www.saes.esiqie.ipn.mx",
    Nombre: "ESIQIE"
  },
  {
    URL: "http://www.saes.esit.ipn.mx",
    Nombre: "ESIT"
  },
  {
    URL: "http://www.saes.upiig.ipn.mx",
    Nombre: "UPIIG"
  },
  {
    URL: "http://www.saes.upiiz.ipn.mx",
    Nombre: "UPIIZ"
  },
  {
    URL: "http://www.saes.cecyt1.ipn.mx",
    Nombre: "CECyT 1"
  },
  {
    URL: "http://www.saes.cecyt2.ipn.mx",
    Nombre: "CECyT 2"
  },
  {
    URL: "http://www.saes.cecyt3.ipn.mx",
    Nombre: "CECyT 3"
  },
  {
    URL: "http://www.saes.cecyt4.ipn.mx",
    Nombre: "CECyT 4"
  },
  {
    URL: "http://www.saes.cecyt5.ipn.mx",
    Nombre: "CECyT 5"
  },
  {
    URL: "http://www.saes.cecyt6.ipn.mx",
    Nombre: "CECyT 6"
  },
  {
    URL: "http://www.saes.cecyt7.ipn.mx",
    Nombre: "CECyT 7"
  },
  {
    URL: "http://www.saes.cecyt8.ipn.mx",
    Nombre: "CECyT 8"
  },
  {
    URL: "http://www.saes.cecyt9.ipn.mx",
    Nombre: "CECyT 9"
  },
  {
    URL: "http://www.saes.cecyt10.ipn.mx",
    Nombre: "CECyT 10"
  },
  {
    URL: "http://www.saes.cecyt11.ipn.mx",
    Nombre: "CECyT 11"
  },
  {
    URL: "http://www.saes.cecyt12.ipn.mx",
    Nombre: "CECyT 12"
  },
  {
    URL: "http://www.saes.cecyt13.ipn.mx",
    Nombre: "CECyT 13"
  },
  {
    URL: "http://www.saes.cecyt14.ipn.mx",
    Nombre: "CECyT 14"
  },
  {
    URL: "http://www.saes.cecyt15.ipn.mx",
    Nombre: "CECyT 15"
  },
  {
    URL: "http://www.saes.cecyt16.ipn.mx",
    Nombre: "CECyT 16"
  },
  {
    URL: "http://www.saes.cecyt17.ipn.mx",
    Nombre: "CECyT 17"
  },
  {
    URL: "http://www.saes.cetyt18.ipn.mx",
    Nombre: "CECYT 18"
  },
  {
    URL: "http://www.saes.cet1.ipn.mx",
    Nombre: "CET 1"
  }
];

export default escuelas
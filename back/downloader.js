var http = require("https")
var sendPdf = function (res, url) {
   console.log(url)
   http.get(url, function (response) {
      if (response.statusCode === 200) {
         var chunks = [];
         response.on('data', function (chunk) {
            console.log('downloading');
            chunks.push(chunk);
         });
         response.on("end", function () {
            console.log('downloaded');
            var jsfile = new Buffer.concat(chunks).toString('base64');
            console.log('converted to base64');
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "X-Requested-With");
            res.header('content-type', 'application/pdf');
            res.send({
               status: 200,
               pdf: jsfile
            })
         });
      } else {
         res.send({
            status: 404
         })
      }
   }).on("error", function () {
      res.send({
         status: 404
      })
   });
}

module.exports = {
   sendPdf
}
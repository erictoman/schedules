const path = require("path");
const escuelas = require("./escuelas.js")
const downloader = require("./downloader.js")
const loginGet = function (req, res) {
  res.sendFile(path.join(__dirname + './../public/index.html'));
}

const horarioPost = function (req, res) {
  var url = escuelas[req.body.escuela].URL + "/PDF/Alumnos/Reinscripciones/" + req.body.boleta + "-ComprobanteHorario.pdf"
  downloader.sendPdf(res, url.replace("http", "https"))
}

const horarioGet = function (req, res) {
  res.send(req.data)
}



module.exports = {
  loginGet,
  horarioPost,
  horarioGet
}